package org.g7i.four.metrics.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.g7i.four.metrics.models.service.IIssueService;
import org.g7i.four.metrics.models.service.IMetricService;
import org.g7i.four.metrics.models.service.IPipelineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
class MetricControllerTest {

	@Inject
	IMetricService metricService;

	@InjectMock
	IIssueService issueService;

	@InjectMock
	IPipelineService pipelineService;

	@ConfigProperty(name = "statistics.deployment.frequency")
	String statisticDeploymentFrequency;
	@ConfigProperty(name = "statistics.lead.time.to.change")
	String statisticLeadTime;
	@ConfigProperty(name = "statistics.mttr")
	String statisticMttr;

	@BeforeEach
	void setup() {
		MockUtils utils = new MockUtils();
		Mockito.when(issueService.getIncidentsRaisedInPeriod()).thenReturn(utils.getIssues("incidentsRaised"));
		Mockito.when(issueService.getIncidentsClosedInPeriod()).thenReturn(utils.getIssues("incidentsClosed"));
		Mockito.when(issueService.getAllIssuesRaisedInPeriod()).thenReturn(utils.getIssues("issuesRaised"));
		Mockito.when(issueService.getAllIssuesClosedInPeriod()).thenReturn(utils.getIssues("issuesClosed"));
		Mockito.when(pipelineService.getPipelinesInPeriod(true, true)).thenReturn(utils.getBuilds());
		Mockito.when(pipelineService.getBuildsTimeInPeriod(true)).thenReturn(utils.getBuildsTime());
		QuarkusMock.installMockForType(issueService, IIssueService.class);
		QuarkusMock.installMockForType(pipelineService, IPipelineService.class);
	}

	@Test
	void testCalculateLeadTimeEndpoint() {
		String result = "";
		if (this.statisticDeploymentFrequency.equalsIgnoreCase("average")) {
			result = "6.558623353200002E8";
		} else {
			result = "1.766629915E8";
		}
		given()
			.when().get("/metrics/leadtime")
			.then()
				.statusCode(200)
				.body(is(result));
	}

	@Test
	void testCalculateMTTREndpoint() {
		String result = "";
		if (this.statisticMttr.equalsIgnoreCase("average")) {
			result = "1.0074558805652174E9";
		} else {
			result = "1.77049452E8";
		}
		given()
			.when().get("/metrics/MTTR")
			.then()
				.statusCode(200)
				.body(is(result));
	}

	@Test
	void testCalculateFailureRateEndpoint() {
		given()
			.when().get("/metrics/failurerate")
			.then()
				.statusCode(200)
				.body(is("450.0"));
	}

	@Test
	void testCalculateDeploymentFrecuencyEndpoint() {
		String result = "";
		if (this.statisticDeploymentFrequency.equalsIgnoreCase("average")) {
			result = "8.592952076666666E8";
		} else {
			result = "8.2790658E8";
		}
		given()
			.when().get("/metrics/deploymentfrequency")
			.then()
				.statusCode(200)
				.body(is(result));
	}

	@Test
	void testCalculateDeploymentFrecuencyEndpointNotFound() {
		given()
			.when().get("/metrics/another/path/deploymentfrequency")
			.then()
				// URL Not Found
				.statusCode(404);
	}

}
