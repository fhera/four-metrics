package org.g7i.four.metrics.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.io.IOUtils;
import org.g7i.four.metrics.models.entities.issue.Issue;
import org.g7i.four.metrics.models.entities.stash.Pipeline;

import lombok.extern.java.Log;

@ApplicationScoped
@Log
public class MockUtils {

    /**
     * Get list of issues by type
     * 
     * @param type Type of issues: issuesRaised, issuesClosed, incidentsRaised,
     *             incidentsClosed
     * @return List of issues
     */
    List<Issue> getIssues(String type) {
        String json;
        List<Issue> issues = new LinkedList<>();
        ObjectMapper mapper = new ObjectMapper();

        String filePath = "src/test/java/org/g7i/four/metrics/files/";
        if (type.equals("issuesRaised")) {
            filePath += "issuesRaised.json";
        } else if (type.equals("issuesClosed")) {
            filePath += "issuesClosed.json";
        } else if (type.equals("incidentsRaised")) {
            filePath += "incidentsRaised.json";
        } else if (type.equals("incidentsClosed")) {
            filePath += "incidentsClosed.json";
        }

        try (FileInputStream inputStream = new FileInputStream(filePath)) {
            json = IOUtils.toString(inputStream, "UTF-8");
            issues.addAll(
                    mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, Issue.class)));
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
        return issues;
    }

    /**
     * Get list of builds
     * 
     * @return List of builds
     */
    List<Pipeline> getBuilds() {
        String json;
        List<Pipeline> pipelines = new LinkedList<>();
        ObjectMapper mapper = new ObjectMapper();

        String filePath = "src/test/java/org/g7i/four/metrics/files/builds.json";

        try (FileInputStream fis = new FileInputStream(filePath)) {
            json = IOUtils.toString(fis, "UTF-8");
            pipelines.addAll(mapper.readValue(json,
                    mapper.getTypeFactory().constructCollectionType(List.class, Pipeline.class)));
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
        return pipelines;
    }

    /**
     * Get list of time between builds
     * 
     * @return List of time between builds
     * @throws URISyntaxException
     */
    List<Long> getBuildsTime() {
        String json;
        List<Long> buildsTime = new LinkedList<>();
        ObjectMapper mapper = new ObjectMapper();

        String filePath = "src/test/java/org/g7i/four/metrics/files/buildsTime.json";

        try (FileInputStream fis = new FileInputStream(filePath)) {
            json = IOUtils.toString(fis, "UTF-8");
            buildsTime.addAll(
                    mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, Long.class)));
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
        return buildsTime;
    }

}
