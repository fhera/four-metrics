package org.g7i.four.metrics.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.g7i.four.metrics.models.service.IIssueService;
import org.g7i.four.metrics.models.service.IMetricService;
import org.g7i.four.metrics.models.service.IPipelineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.opentest4j.AssertionFailedError;

import io.quarkus.test.junit.QuarkusMock;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;

@QuarkusTest
class MetricServiceTest {

	@Inject
	IMetricService metricService;

	@InjectMock
	IIssueService issueService;

	@InjectMock
	IPipelineService pipelineService;

	@ConfigProperty(name = "statistics.deployment.frequency")
	String statisticDeploymentFrequency;
	@ConfigProperty(name = "statistics.lead.time.to.change")
	String statisticLeadTime;
	@ConfigProperty(name = "statistics.mttr")
	String statisticMttr;

	@BeforeEach
	void setup() {
		MockUtils utils = new MockUtils();
		Mockito.when(issueService.getIncidentsRaisedInPeriod()).thenReturn(utils.getIssues("incidentsRaised"));
		Mockito.when(issueService.getIncidentsClosedInPeriod()).thenReturn(utils.getIssues("incidentsClosed"));
		Mockito.when(issueService.getAllIssuesRaisedInPeriod()).thenReturn(utils.getIssues("issuesRaised"));
		Mockito.when(issueService.getAllIssuesClosedInPeriod()).thenReturn(utils.getIssues("issuesClosed"));
		Mockito.when(pipelineService.getPipelinesInPeriod(true, true)).thenReturn(utils.getBuilds());
		Mockito.when(pipelineService.getBuildsTimeInPeriod(true)).thenReturn(utils.getBuildsTime());
		QuarkusMock.installMockForType(issueService, IIssueService.class);
		QuarkusMock.installMockForType(pipelineService, IPipelineService.class);
	}

	@Test
	void testCalculateIncidentsRaisedThenSizeOfIncidentMustBeCoincident() {
		Integer result = metricService.calculateIncidentsRaised();
		assertEquals(18, result);
	}

	@Test
	void testCalculateIncidentsRaisedThenSizeOfIncidentMustNotBeCoincident() {
		Integer result = metricService.calculateIncidentsRaised();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(2, result);
		});
	}

	@Test
	void testCalculateIncidentsClosedThenSizeOfIncidentMustBeCoincident() {
		Integer result = metricService.calculateIncidentsClosed();
		System.out.println(result);
		assertEquals(23, result);
	}

	@Test
	void testCalculateIncidentsClosedThenSizeOfIncidentMustNotBeCoincident() {
		Integer result = metricService.calculateIncidentsClosed();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(3, result);
		});
	}

	@Test
	void testCalculateLeadTimeThenShouldBeSame() {
		Double result = metricService.calculateLeadTime();
		if (this.statisticDeploymentFrequency.equalsIgnoreCase("average")) {
			assertEquals(6.558623353200002E8, result);
		} else {
			assertEquals(1.766629915E8, result);
		}
	}

	@Test
	void testCalculateLeadTimeThenShouldNotBeSame() {
		Double result = metricService.calculateLeadTime();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(6.558623353200002E9, result);
		});
	}

	@Test
	void testCalculateMTTRThenShouldBeSame() {
		Double result = metricService.calculateMTTR();
		if (this.statisticMttr.equalsIgnoreCase("average")) {
			assertEquals(1.0074558805652174E9, result);
		} else {
			assertEquals(1.77049452E8, result);
		}
	}

	@Test
	void testCalculateMTTRThenShouldNotBeSame() {
		Double result = metricService.calculateMTTR();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(6., result);
		});
	}

	@Test
	void testCalculateFailureRateThenShouldBeSame() {
		Double result = metricService.calculateFailureRate();
		assertEquals(450.0, result);
	}

	@Test
	void testCalculateFailureRateThenShouldNotBeSame() {
		Double result = metricService.calculateFailureRate();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(6., result);
		});
	}

	@Test
	void testCalculateDeploymentFrequencyThenShouldBeSame() {
		Double result = metricService.calculateDeploymentFrequency();
		if (this.statisticDeploymentFrequency.equalsIgnoreCase("average")) {
			assertEquals(8.592952076666666E8, result);
		} else {
			assertEquals(8.2790658E8, result);
		}
	}

	@Test
	void testCalculateDeploymentFrequencyThenShouldNotBeSame() {
		Double result = metricService.calculateDeploymentFrequency();
		assertThrows(AssertionFailedError.class, () -> {
			assertEquals(6., result);
		});
	}
}
