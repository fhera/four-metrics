package org.g7i.four.metrics.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Page<T> {

	private Integer size;
	@NonNull
	private List<T> values;

	public boolean isLastPage() {
		return this.values.isEmpty();
	}

}
