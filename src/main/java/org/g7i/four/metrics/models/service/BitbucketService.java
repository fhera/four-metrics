package org.g7i.four.metrics.models.service;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.g7i.four.metrics.models.entities.stash.Commit;
import org.g7i.four.metrics.models.restservices.BitbucketRestService;
import org.g7i.four.metrics.util.Page;

import lombok.NonNull;

@ApplicationScoped
public class BitbucketService implements IGitService {

	@Inject
	@RestClient
	BitbucketRestService bitbucketRestService;

	@ConfigProperty(name = "period.days")
	Integer period;
	@ConfigProperty(name = "bitbucket.workspace")
	String workspace;
	@ConfigProperty(name = "bitbucket.repository")
	String repository;

	/**
	 * Catch all commits
	 * 
	 * @return List of commits from repository
	 */
	public List<Commit> getAllCommits() {
		List<Commit> result = new LinkedList<>();
		Page<Commit> pagedCommits = this.bitbucketRestService.getCommits(workspace, repository);
		Integer i = 2;
		while (!pagedCommits.isLastPage()) {
			result.addAll(pagedCommits.getValues());
			pagedCommits = this.bitbucketRestService.getCommits(workspace, repository, i);
			i++;
		}
		return result;
	}

	/**
	 * Catch all commits
	 * 
	 * @param search Search word in all commits of period
	 * @return List of commits from repository
	 */
	public List<Commit> getAllCommits(String search) {
		List<Commit> result = this.getAllCommits();
		if (search != null) {
			result.removeIf(commit -> !commit.getMessage().toLowerCase().contains(search.toLowerCase()));
		}

		return result;
	}

	/**
	 * Catch all commits in period
	 * 
	 * @param search Search word in all commits of period
	 * @return List of commits from repository
	 */
	public List<Commit> getCommitsInPeriod() {
		List<Commit> result = new LinkedList<>();
		Integer i = 1;
		var calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -period);
		Page<Commit> pagedCommits = this.bitbucketRestService.getCommits(workspace, repository);
		while (!pagedCommits.isLastPage()) {
			result.addAll(pagedCommits.getValues());
			// Catch all commits in period
			if (result.get(result.size() - 1).getDate().before(calendar.getTime())) {
				break;
			}
			i++;
			pagedCommits = this.bitbucketRestService.getCommits(workspace, repository, i);
		}
		return result;
	}

	/**
	 * Catch all commits in period
	 * 
	 * @param search Search word in all commits of period
	 * @return List of commits from repository
	 */
	public List<Commit> getCommitsInPeriod(String search) {
		List<Commit> result = this.getCommitsInPeriod();
		if (search != null) {
			result.removeIf(commit -> !commit.getMessage().toLowerCase().contains(search.toLowerCase()));
		}
		return result;
	}

	/**
	 * Catch all merged commits in period
	 * 
	 * @return List of commits from repository
	 */
	public List<Commit> getMergedCommitsInPeriod() {
		List<Commit> result = this.getCommitsInPeriod();
		result.removeIf(commit -> !commit.getMessage().toLowerCase().contains("merged"));
		return result;
	}

	/**
	 * Get a commit
	 * 
	 * @param commitId Hash of commit
	 * @return Commit
	 */
	public Commit getCommit(@NonNull String commitId) {
		Page<Commit> pagedCommit = this.bitbucketRestService.getCommit(workspace, repository, commitId);
		return pagedCommit.getValues().get(0);
	}

}
