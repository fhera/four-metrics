package org.g7i.four.metrics.models.service;

import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.g7i.four.metrics.models.entities.issue.Issue;

import com.google.common.math.Quantiles;
import com.google.common.math.Stats;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class MetricService implements IMetricService {

	@Inject
	IIssueService issueService;
	@Inject
	IGitService stashService;
	@Inject
	IPipelineService pipelineService;

	@ConfigProperty(name = "statistics.deployment.frequency", defaultValue = "average")
	String statisticDeploymentFrequency;
	@ConfigProperty(name = "statistics.lead.time.to.change", defaultValue = "average")
	String statisticLeadTime;
	@ConfigProperty(name = "statistics.mttr", defaultValue = "average")
	String statisticMttr;

	/**
	 * Calculate number of incidents raised.
	 * 
	 * @return Number of incidents raised, must be an Integer
	 */
	public Integer calculateIncidentsRaised() {
		return this.issueService.getIncidentsRaisedInPeriod().size();
	}

	/**
	 * Calculate number of incidents closed.
	 * 
	 * @return Number of incidents closed, must be an Integer
	 */
	public Integer calculateIncidentsClosed() {
		return this.issueService.getIncidentsClosedInPeriod().size();
	}

	/**
	 * Calculate number of deployments.
	 * 
	 * @return Number of deployments in period, must be an Integer
	 */
	public Integer calculateNumberOfDeployments() {
		return this.pipelineService.getPipelinesInPeriod(true, true).size();
	}

	/**
	 * Calculate Deployment Frequency. Deployment Frequency is the number of
	 * deployed builds in production environment
	 * 
	 * @return Deployment Frequency, must be an Double
	 */
	public Double calculateDeploymentFrequency() {
		var builds = this.pipelineService.getBuildsTimeInPeriod(true);

		return this.statisticDeploymentFrequency.equals("median") ? //
				Quantiles.median().compute(builds) : Stats.meanOf(builds);
	}

	/**
	 * Calculate Failure Rate. Failure Rate is a ratio between incidents raised and
	 * number of deployed builds
	 * 
	 * @return
	 */
	public Double calculateFailureRate() {
		return this.issueService.getIncidentsRaisedInPeriod().size()
				/ Double.valueOf(this.calculateNumberOfDeployments()) * 100;
	}

	/**
	 * Calculate Lead Time. Lead Time is the time between opening an incident and
	 * closing it.
	 * 
	 * @return Lead Time in milliseconds
	 */
	public Double calculateLeadTime() {
		var allIssuesClosed = this.issueService.getAllIssuesClosedInPeriod().stream()
				// Calculate resolution of all incidents in milliseconds
				.map((Issue issue) -> {
					var resolutionDate = issue.getFields().getResolutionDate();
					return (resolutionDate.getTime() - issue.getFields().getCreated().getTime());
				}).collect(Collectors.toList());

		return this.statisticDeploymentFrequency.equals("median") ? //
				Quantiles.median().compute(allIssuesClosed) : Stats.meanOf(allIssuesClosed);
	}

	/**
	 * Calculate Lead Time. Lead Time is time between open a incident until this
	 * incident is closed.
	 * 
	 * @return MTTR in milliseconds
	 */
	public Double calculateMTTR() {
		var incidentClosed = this.issueService.getIncidentsClosedInPeriod().stream()
				// Calculate resolution incident in milliseconds
				.map((Issue incident) -> {
					var resolutionDate = incident.getFields().getResolutionDate();
					return (resolutionDate.getTime() - incident.getFields().getCreated().getTime());
				}).collect(Collectors.toList());
		return this.statisticDeploymentFrequency.equals("median") ? //
				Quantiles.median().compute(incidentClosed) : Stats.meanOf(incidentClosed);
	}

}
