package org.g7i.four.metrics.models.entities.issue;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Issue
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Issue {

	@NonNull
	Integer id;
	@NonNull
	String key;
	@NonNull
	Fields fields;

	String self;

	@Data
	@RequiredArgsConstructor
	public static class Fields {
		Date created;
		@JsonProperty("resolutiondate")
		Date resolutionDate;

		@JsonProperty("issuetype")
		IssueType type;

		@Data
		@RequiredArgsConstructor
		public static class IssueType {
			String name;
		}
	}
}