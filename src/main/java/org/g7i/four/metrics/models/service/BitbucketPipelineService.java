package org.g7i.four.metrics.models.service;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.g7i.four.metrics.models.entities.stash.Pipeline;
import org.g7i.four.metrics.models.restservices.BitbucketRestService;
import org.g7i.four.metrics.util.Page;

import one.util.streamex.StreamEx;

//@Log
@ApplicationScoped
public class BitbucketPipelineService implements IPipelineService {

	private static final Pattern versionRegex = Pattern.compile("\\d+.\\d+.\\d+");

	@Inject
	@RestClient
	BitbucketRestService bitbucketRestService;

	@ConfigProperty(name = "period.days")
	Integer period;
	@ConfigProperty(name = "bitbucket.workspace")
	String workspace;
	@ConfigProperty(name = "bitbucket.repository")
	String repository;

	/**
	 * Catch all pipelines in period
	 * 
	 * @param inPeriod   Filter pipelines between default period and now
	 * @param onlyBuilds Filter pipelines by builds
	 * @return List of pipelines
	 */
	public List<Pipeline> getPipelinesInPeriod(boolean inPeriod, boolean onlyBuilds) {
		List<Pipeline> result = new LinkedList<>();
		var calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -period);
		Page<Pipeline> pagedCommits = this.bitbucketRestService.getPipelines(workspace, repository);
		Integer i = (int) Math.ceil(pagedCommits.getSize().doubleValue() / 10);
		while (i != 1) {
			pagedCommits = this.bitbucketRestService.getPipelines(workspace, repository, i);
			var pipelines = pagedCommits.getValues();
			if (onlyBuilds) {
				var builds = getOnlyBuilds(pipelines);
				for (Pipeline build : builds) {
					// Catch all builds in period
					if (inPeriod && build.getCreated().after(calendar.getTime())) {
						result.add(build);
					} else {
						break;
					}
				}
			} else {
				result.addAll(pipelines);
			}
			i--;
			// Catch all pipelines in period
			if (inPeriod && pipelines.get(pipelines.size() - 1).getCreated().before(calendar.getTime())) {
				break;
			}
		}
		return result;
	}

	/**
	 * List of time that all builds made in the established period of time are put
	 * into production
	 * 
	 * @param inPeriod Filter pipelines between default period and now
	 * @return List of pipelines
	 */
	public List<Long> getBuildsTimeInPeriod(boolean inPeriod) {
		List<Pipeline> builds = this.getPipelinesInPeriod(inPeriod, true);

		return StreamEx.of(
				// Get ordered builds of date completed
				builds.stream().sorted(Comparator.comparing(Pipeline::getCompleted))
						// Map to date completed
						.map(Pipeline::getCompleted)
						// Map to milliseconds
						.map(Date::getTime))
				// Pairwise differences
				.pairMap((p1, p2) -> p2 - p1).toList();
	}

	/**
	 * Ancillary method to get pipelines that are builds to pro
	 * 
	 * @param pipelines
	 * @return Lost of pipelines
	 */
	private static List<Pipeline> getOnlyBuilds(List<Pipeline> pipelines) {
		List<Pipeline> result = new LinkedList<>();
		for (Pipeline pipeline : pipelines) {
			// Get only successfully builds
			try {
				if ("SUCCESSFUL".equals(pipeline.getState().getResult().getName())
						&& versionRegex.matcher(pipeline.getTarget().getName()).matches()) {
					result.add(pipeline);
				}
			} catch (Exception e) {
			}
		}
		return result;
	}
}
