package org.g7i.four.metrics.models.service;

public interface IMetricService {

	Integer calculateIncidentsRaised();

	Integer calculateIncidentsClosed();

	Integer calculateNumberOfDeployments();

	Double calculateDeploymentFrequency();

	Double calculateFailureRate();

	Double calculateLeadTime();

	Double calculateMTTR();

}
