package org.g7i.four.metrics.models.service;

import java.util.List;

import org.g7i.four.metrics.models.entities.stash.Commit;

public interface IGitService {

	List<Commit> getAllCommits();

	List<Commit> getAllCommits(String search);

	List<Commit> getCommitsInPeriod();

	List<Commit> getCommitsInPeriod(String search);

	List<Commit> getMergedCommitsInPeriod();

	Commit getCommit(String commitId);

}
