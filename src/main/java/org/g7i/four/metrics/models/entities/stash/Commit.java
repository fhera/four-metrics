package org.g7i.four.metrics.models.entities.stash;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Commit {

	@NonNull
	@JsonProperty("hash")
	String id;

	@NonNull
	Date date;

	@NonNull
	String message;

}
