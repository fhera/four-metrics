package org.g7i.four.metrics.models.entities.issue;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * SearchIssue
 */
@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchIssue {

	@NonNull
	Integer startAt;
	@NonNull
	Integer maxResults;
	@NonNull
	Integer total;
	@NonNull
	List<Issue> issues;

}