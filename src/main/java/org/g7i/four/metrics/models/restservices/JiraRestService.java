package org.g7i.four.metrics.models.restservices;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.g7i.four.metrics.models.entities.issue.SearchIssue;

@Path("/rest/api/latest")
@RegisterRestClient
@RegisterClientHeaders(JiraRestServiceHeaderFactory.class)
public interface JiraRestService {

	int MAXRETRIES = 5;

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	SearchIssue getSearchJQL(@QueryParam("jql") String jql);

}
