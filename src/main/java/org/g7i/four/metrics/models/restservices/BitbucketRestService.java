package org.g7i.four.metrics.models.restservices;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.faulttolerance.Retry;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.g7i.four.metrics.models.entities.stash.Commit;
import org.g7i.four.metrics.models.entities.stash.Pipeline;
import org.g7i.four.metrics.util.Page;

@Path("/2.0")
@RegisterRestClient
@RegisterClientHeaders(BitbucketRestServiceHeaderFactory.class)
public interface BitbucketRestService {

	int MAXRETRIES = 5;

	@GET
	@Path("/repositories/{workspace}/{repo}/commits")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	Page<Commit> getCommits(@PathParam("workspace") String workspace, @PathParam("repo") String repository,
			@QueryParam("page") Integer page);

	@GET
	@Path("/repositories/{workspace}/{repo}/commits")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	Page<Commit> getCommits(@PathParam("workspace") String workspace, @PathParam("repo") String repository);

	@GET
	@Path("/repositories/{workspace}/{repo}/pipelines/")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	Page<Pipeline> getPipelines(@PathParam("workspace") String workspace, @PathParam("repo") String repository);

	@GET
	@Path("/repositories/{workspace}/{repo}/pipelines/")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	Page<Pipeline> getPipelines(@PathParam("workspace") String workspace, @PathParam("repo") String repository,
			@QueryParam("page") Integer page);

	@GET
	@Path("/repositories/{workspace}/{repo}/commits/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Retry(maxRetries = MAXRETRIES)
	Page<Commit> getCommit(@PathParam("workspace") String workspace, @PathParam("repo") String repository,
			@PathParam("id") String commitId);

}
