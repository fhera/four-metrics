package org.g7i.four.metrics.models.service;

import java.util.List;

import org.g7i.four.metrics.models.entities.issue.Issue;

public interface IIssueService {

	List<Issue> getAllIssuesClosedInPeriod();

	List<Issue> getAllIssuesRaisedInPeriod();

	List<Issue> getIncidentsClosedInPeriod();

	List<Issue> getIncidentsRaisedInPeriod();

}
