package org.g7i.four.metrics.models.entities.stash;

import java.util.Date;

import org.g7i.four.metrics.models.dtos.stash.CommitDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pipeline {

	@NonNull
	State state;

	@NonNull
	@JsonProperty("created_on")
	Date created;

	@NonNull
	@JsonProperty("completed_on")
	Date completed;

	@NonNull
	Target target;

	@Data
	@NoArgsConstructor
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class State {
		@NonNull
		private Result result;

		@Data
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class Result {
			private String name;
		}
	}

	@Data
	@NoArgsConstructor
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Target {
		@JsonProperty("ref_type")
		private String tipo;
		@JsonProperty("ref_name")
		private String name;
		private CommitDto commit;
	}

}
