package org.g7i.four.metrics.models.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.g7i.four.metrics.models.entities.issue.Issue;
import org.g7i.four.metrics.models.entities.issue.SearchIssue;
import org.g7i.four.metrics.models.restservices.JiraRestService;

import one.util.streamex.StreamEx;

@ApplicationScoped
public class IssueService implements IIssueService {

	@ConfigProperty(name = "incident.jql")
	String incidentJql;
	@ConfigProperty(name = "period.days")
	Integer period;

	@Inject
	@RestClient
	JiraRestService jiraRestService;

	/**
	 * Catch all issues closed in period
	 * 
	 * @return List of issues closed
	 */
	public List<Issue> getAllIssuesClosedInPeriod() {
		SearchIssue search = this.jiraRestService
				.getSearchJQL(String.format("%s AND resolutiondate >= -%dd", this.incidentJql, this.period));
		return search.getIssues();
	}

	/**
	 * Catch all issues raised in period
	 * 
	 * @return List of issues raised
	 */
	public List<Issue> getAllIssuesRaisedInPeriod() {
		SearchIssue search = this.jiraRestService
				.getSearchJQL(String.format("%s AND created >= -%dd", this.incidentJql, this.period));
		return search.getIssues();
	}

	/**
	 * Catch all incidents closed in period. Incidents are bug-type issues.
	 * 
	 * @return List of incident closed
	 */
	public List<Issue> getIncidentsClosedInPeriod() {
		SearchIssue search = this.jiraRestService.getSearchJQL(
				String.format("%s AND type = Bug AND resolutiondate >= -%dd", this.incidentJql, this.period));
		return StreamEx.of(search.getIssues())
				// Get error type issues
				.filter(issue -> issue.getFields().getType().getName().equalsIgnoreCase("Error")).toList();
	}

	/**
	 * Catch all incidents raised in period. Incidents are bug-type issues.
	 * 
	 * @return List of incident raised
	 */
	public List<Issue> getIncidentsRaisedInPeriod() {
		SearchIssue search = this.jiraRestService
				.getSearchJQL(String.format("%s AND type = Bug AND created >= -%dd", this.incidentJql, this.period));
		return StreamEx.of(search.getIssues())
				// Get error type issues
				.filter(issue -> issue.getFields().getType().getName().equalsIgnoreCase("Error")).toList();
	}

}
