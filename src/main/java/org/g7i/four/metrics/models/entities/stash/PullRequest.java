package org.g7i.four.metrics.models.entities.stash;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data(staticConstructor = "of")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PullRequest {

	Integer id;
	Date createdDate;
	Date closedDate;
}
