package org.g7i.four.metrics.models.service;

import java.util.List;

import org.g7i.four.metrics.models.entities.stash.Pipeline;

public interface IPipelineService {

	List<Pipeline> getPipelinesInPeriod(boolean inPeriod, boolean onlyBuilds);

	List<Long> getBuildsTimeInPeriod(boolean inPeriod);
}
