package org.g7i.four.metrics.scheduler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.metrics.MetricRegistry;
import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Gauge;
import org.g7i.four.metrics.models.service.IMetricService;

import io.quarkus.scheduler.Scheduled;

/**
 * This class catch all metrics each 24 hours.
 */
@ApplicationScoped
public class MetricsScheduler {

	private Integer numberOfDeployments;
	private Double deploymentFrequency;
	private Double leadTime;
	private Double mttr;
	private Double failureRate;
	private Integer incidentsClosed;
	private Integer incidentsRaised;

	@Inject
	MetricRegistry registry;

	@Inject
	IMetricService metricService;

	/**
	 * Scheduler method. Each x time to get new metrics.
	 */
	@Scheduled(every = "{metrics.scheduler}")
	void getMetrics() {
		try {
			this.incidentsRaised = this.getIncidentsRaised();
			this.incidentsClosed = this.getIncidentsClosed();
			this.numberOfDeployments = this.getNumberOfDeployments();
			this.deploymentFrequency = this.getDeploymentFrequency();
			this.failureRate = this.getFailureRate();
			this.leadTime = this.getLeadTime();
			this.mttr = this.getMTTR();
		} catch (Exception e) {
		}
	}

	@Gauge(name = "incidents.closed", unit = MetricUnits.NONE)
	public Integer getIncidentsClosed() {
		return this.incidentsClosed == null ? this.metricService.calculateIncidentsClosed() : this.incidentsClosed;
	}

	@Gauge(name = "incidents.raised", unit = MetricUnits.NONE)
	public Integer getIncidentsRaised() {
		return this.incidentsRaised == null ? this.metricService.calculateIncidentsRaised() : this.incidentsRaised;
	}

	@Gauge(name = "number.deployments", unit = MetricUnits.NONE)
	public Integer getNumberOfDeployments() {
		return this.numberOfDeployments == null ? this.metricService.calculateNumberOfDeployments()
				: this.numberOfDeployments;
	}

	@Gauge(name = "deployment.frequency", unit = MetricUnits.MILLISECONDS)
	public Double getDeploymentFrequency() {
		return this.deploymentFrequency == null ? this.metricService.calculateDeploymentFrequency()
				: this.deploymentFrequency;
	}

	@Gauge(name = "lead.time", unit = MetricUnits.MILLISECONDS)
	public Double getLeadTime() {
		return this.leadTime == null ? this.metricService.calculateLeadTime() : this.leadTime;
	}

	@Gauge(name = "mttr", unit = MetricUnits.MILLISECONDS)
	public Double getMTTR() {
		return this.mttr == null ? this.metricService.calculateMTTR() : this.mttr;
	}

	@Gauge(name = "failure.rate", unit = MetricUnits.PERCENT)
	public Double getFailureRate() {
		return this.failureRate == null ? this.metricService.calculateFailureRate() : this.failureRate;
	}

}
