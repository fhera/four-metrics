package org.g7i.four.metrics.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.g7i.four.metrics.models.entities.issue.Issue;
import org.g7i.four.metrics.models.service.IIssueService;

@Path("/issue")
public class IssueController {

	@Inject
	IIssueService issueService;

	@GET
	@Path("/closed")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Issue> getAllIssuesClosedInPeriod() {
		return this.issueService.getAllIssuesClosedInPeriod();
	}

	@GET
	@Path("/closed/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Integer getAllIssuesClosedCountInPeriod() {
		return this.issueService.getAllIssuesClosedInPeriod().size();
	}

	@GET
	@Path("/raised")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Issue> getAllIssuesRaisedInPeriod() {
		return this.issueService.getAllIssuesRaisedInPeriod();
	}

	@GET
	@Path("/raised/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Integer getAllIssuesRaisedCountInPeriod() {
		return this.issueService.getAllIssuesRaisedInPeriod().size();
	}

	@GET
	@Path("/incident/closed")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Issue> getIncidentsClosedInPeriod() {
		return this.issueService.getIncidentsClosedInPeriod();
	}

	@GET
	@Path("/incident/closed/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Integer getIncidentsClosedCountInPeriod() {
		return this.issueService.getIncidentsClosedInPeriod().size();
	}

	@GET
	@Path("/incident/raised")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Issue> getIncidentsRaisedInPeriod() {
		return this.issueService.getIncidentsRaisedInPeriod();
	}

	@GET
	@Path("/incident/raised/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Integer getIncidentsRaisedCountInPeriod() {
		return this.issueService.getIncidentsRaisedInPeriod().size();
	}

}