package org.g7i.four.metrics.controllers;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.g7i.four.metrics.models.entities.stash.Commit;
import org.g7i.four.metrics.models.entities.stash.Pipeline;
import org.g7i.four.metrics.models.service.IGitService;
import org.g7i.four.metrics.models.service.IMetricService;
import org.g7i.four.metrics.models.service.IPipelineService;

@Path("/git")
public class GitController {

    @Inject
    IGitService stashService;
    @Inject
    IPipelineService pipelineService;
    @Inject
    IMetricService metricService;

    @GET
    @Path("/commits")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Commit> getAllCommits(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod,
            @QueryParam("search") String search) {
        return inPeriod ? this.stashService.getCommitsInPeriod(search) : this.stashService.getAllCommits(search);
    }

    @GET
    @Path("/commits/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Commit getCommit(@PathParam("id") String commitId) {
        return this.stashService.getCommit(commitId);
    }

    @GET
    @Path("/commits/merged")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Commit> getMergedCommitsInPeriod() {
        return this.stashService.getMergedCommitsInPeriod();
    }

    @GET
    @Path("/commits/merged/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getMergedCommitsCountInPeriod() {
        return this.stashService.getMergedCommitsInPeriod().size();
    }

    @GET
    @Path("/pipelines")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pipeline> getPipelinesInPeriod(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod) {
        return this.pipelineService.getPipelinesInPeriod(inPeriod, false);
    }

    @GET
    @Path("/pipelines/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getNumberPipelinesInPeriod(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod) {
        return this.pipelineService.getPipelinesInPeriod(inPeriod, false).size();
    }

    @GET
    @Path("/pipelines/builds")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Pipeline> getBuildsInPeriod(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod) {
        return this.pipelineService.getPipelinesInPeriod(inPeriod, true);
    }

    @GET
    @Path("/pipelines/builds/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer getNumberBuildsInPeriod(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod) {
        return this.pipelineService.getPipelinesInPeriod(inPeriod, true).size();
    }

    @GET
    @Path("/pipelines/builds/time")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Long> getBuildsTimeInPeriod(@DefaultValue("true") @QueryParam("inperiod") boolean inPeriod) {
        return this.pipelineService.getBuildsTimeInPeriod(inPeriod);
    }

}