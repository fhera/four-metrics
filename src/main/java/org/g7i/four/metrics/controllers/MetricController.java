package org.g7i.four.metrics.controllers;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.g7i.four.metrics.models.service.IMetricService;

@Path("/metrics")
public class MetricController {

	@Inject
	IMetricService metricService;

	@GET
	@Path("/deploymentfrequency")
	@Produces(MediaType.APPLICATION_JSON)
	public Double getDeploymentFrequency() {
		return this.metricService.calculateDeploymentFrequency();
	}

	@GET
	@Path("/leadtime")
	@Produces(MediaType.APPLICATION_JSON)
	public Double getLeadTime() {
		return this.metricService.calculateLeadTime();
	}

	@GET
	@Path("/MTTR")
	@Produces(MediaType.APPLICATION_JSON)
	public Double getMTTR() {
		return this.metricService.calculateMTTR();
	}

	@GET
	@Path("/failurerate")
	@Produces(MediaType.APPLICATION_JSON)
	public Double getFailureRate() {
		return this.metricService.calculateFailureRate();
	}
}