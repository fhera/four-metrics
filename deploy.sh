#!/bin/bash

GREEN='\033[0;32m'
BOLD='\033[1m'
NC='\033[0m' # No Color

printf "${GREEN}Build native Quarkus application.${NC}\n"
printf "${BOLD}This operation is expensive and will take time but it will generate an executable binary in any os...${NC}\n"
./mvnw package -Pnative

printf "${GREEN}Deploy${NC}\n"
docker-compose up -d